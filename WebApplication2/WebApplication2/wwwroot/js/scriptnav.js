jQuery(document).ready(function() {

    var navOffset =jQuery("header").offset().top;
    jQuery("header").wrap('<div class="header-placeholder"></div>');
    jQuery(".header-placeholder").height(jQuery("header").outerHeight());
    jQuery(window).scroll(function() {
    var scrollpos=jQuery(window).scrollTop();
        if(scrollpos>=navOffset){
            jQuery("header").addClass("fixed");

        } else {
            jQuery("header").removeClass("fixed");
        }
        if(scrollpos==0){
            jQuery("header").removeClass("fixed");
        }
    });
});


