﻿// Write your JavaScript code.
var control = false;
function disp() {
    var device = "Tablet";
    if (/Android|iPhone|webOS/i.test(navigator.userAgent)) {
        disp = "Phone";
    } else if (/iPad/i.test(navigator.userAgent)) {
        disp = "Tablet";
    } else if (/Linux i686|Mac68K|Win16|Win32/i.test(navigator.platform)) {
        disp = "Desktop";
    }
    if (control == false) {
        document.getElementById("disp").style.opacity = "1";
        control = true;
    } else if (control == true) {
        document.getElementById("disp").style.opacity = "0";
        control = false;
    }
    document.getElementById("res").value = "dispositivo:" + disp;
    return disp;
}